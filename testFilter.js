const parents = require('ldap-filter')

const filters = [
  'CN=Правление ВТБ,OU=Distribution Groups,OU=Groups,OU=MSK_VTB,DC=region,DC=vtb,DC=ru',
  'CN=Сотрудники Москва (прописка-SAP-ИКС),OU=All_Staff,OU=Distribution Groups,OU=Groups,OU=MSK_VTB,DC=region,DC=vtb,DC=ru'
];


function escapedToHex (str) {
  return str.replace(/\\([0-9a-f](?![0-9a-f])|[^0-9a-f]|$)/gi, function (match, p1) {
    if (!p1) {
      return '\\5c'
    }

    const hexCode = p1.charCodeAt(0).toString(16)
    return '\\' + hexCode
  })
}

function parseString (str) {
  const hexStr = escapedToHex(str)
  const generic = parents.parse(hexStr)
  // The filter object(s) return from ldap-filter.parse lack the toBer/parse
  // decoration that native ldapjs filter possess.  cloneFilter adds that back.
}

// partially stolen from https://github.com/tcort/ldap-escape/blob/master/index.js
const escapeLdapDnValue = (value) => {
  if (!value || typeof value !== 'string') {
    return value;
  }

  const replacements = {
    filter: {
      '\u0000': '\\00', // NUL
      '\u0028': '\\28', // (
      '\u0029': '\\29', // )
      '\u002a': '\\2a', // *
      '\u005c': '\\5c', // \
    }
  };

  return value.replace(/[\u0000\u0028\u0029\u002a\u005c]/gm, (ch) => replacements.filter[ch]);
};

const reverseEscape = (value) => {
  const reverseFilter = {
    '\\00': '\u0000',
    '\\28': '\u0028',
    '\\29': '\u0029',
    '\\2a': '\u002a',
    '\\5c': '\u005c',
  };

  return value.replace(/\\00|\\28|\\29|\\2a|\\5c/gm, (ch) => reverseFilter[ch]);
}

const escapeLdapDn = (dn) => {
  const clearedDn = reverseEscape(dn);
  return clearedDn
    .split(/(?<!\\),/g)
    .map((current, index, arr) => {
      const replaceString = '<==>';
      const formattedString = current.replace(/=/, replaceString);
      const [key, value] = formattedString.split(replaceString);
      return [key, escapeLdapDnValue(value)].join('=');
    })
    .join(',');
};

function parseDistinguishedName(dn) {
  if (! dn) return(dn);

  dn = dn.replace(/"/g, '\\"');
  return(dn.replace('\\,', '\\\\,'));
}

// const dn = 'CN=Члены правления\\, вице-президенты и руководители структурных подр,OU=Distribution Groups,OU=Groups,OU=MSK_VTB,DC=region,DC=vtb,DC=ru';
// escapeLdapDn(dn);

const dns = [
  'CN=Москотлинов Андрей Алексеевич,OU=Практика систем управления талантами и обучением\\5c, Москва г\\5c, На,OU=Практика систем управления талантами и обучением,OU=Центр Сап,OU=Дирекция поддержки и развития систем блока организационных вопро,OU=Users,OU=ООО ИТСК,OU=MSK,OU=Regions,DC=gazprom-neft,DC=local',
    'CN=Москотлинов Андрей Алексеевич,OU=Практика систем управления талантами и обучением\\, Москва г\\, На,OU=Практика систем управления талантами и обучением,OU=Центр Сап,OU=Дирекция поддержки и развития систем блока организационных вопро,OU=Users,OU=ООО ИТСК,OU=MSK,OU=Regions,DC=gazprom-neft,DC=local',
    ...filters,
];

dns.forEach((dn) => {
  console.log(escapeLdapDn(dn) === escapeLdapDn(dn), escapeLdapDn(dn));
})

// filters.forEach((current) => {
//   const parsed = parseDistinguishedName(current);
//   const filter = '(member=' + escapeLdapDn(current) + ')';
//   console.log('filter', JSON.stringify({ filter }));
//   parseString(filter)
// });